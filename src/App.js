import React, { Component } from 'react';
import './App.css';
import KeyBoard from './components/KeyBoard';

class App extends Component {
  constructor(props){
    super(props);
    this.state = this.initialState();
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  initialState(){
    return {
      result: '',
      operators: ['*', '/', '-', '+', '.'],
      lastOperator: '',
      operatorGiven: false
    };
  }

  onKeyPress(key){
    if (key === 'C'){
      this.setState({result: ''});
    }
    else if (key === '='){
      this.setState((prevState) => {
        return {
          result: eval(prevState.result)
        };
      })
    }
    else if (this.state.operators.includes(key)){
      this.setState({operatorGiven: true});
      if (this.state.lastOperator === '')
        this.setState((prevState) => {
          return {
            result: prevState.result + key,
            lastOperator: key
          };
        });
    }
    else this.setState((prevState) => {
      return {
        result: prevState.result + key,
        lastOperator: ''
      };
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-container">
          <div className="App-result">{this.state.result}</div>
          <div className="App-keyboard">
            <KeyBoard
              onPress={this.onKeyPress}
              disabledEqualButton={!this.state.operatorGiven}
              disabledOperators={this.state.lastOperator !== '' || this.state.result === ''}
              disabledZeroButton={this.state.lastOperator === '/'}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
