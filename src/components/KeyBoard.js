import React, { Component } from 'react';
import './KeyBoard.css';

class KeyBoard extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <div>
        <div className="KeyBoard-row">
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">1</button>
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">2</button>
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">3</button>
          <button disabled={this.props.disabledOperators} onClick={(e) => this.props.onPress(e.target.innerText) } type="button">+</button>
        </div>
        <div className="KeyBoard-row">
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">4</button>
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">5</button>
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">6</button>
          <button disabled={this.props.disabledOperators} onClick={(e) => this.props.onPress(e.target.innerText) } type="button">-</button>
        </div>
        <div className="KeyBoard-row">
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">7</button>
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">8</button>
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">9</button>
          <button disabled={this.props.disabledOperators} onClick={(e) => this.props.onPress(e.target.innerText) } type="button">*</button>
        </div>
        <div className="KeyBoard-row">
          <button onClick={(e) => this.props.onPress(e.target.innerText) } type="button">C</button>
          <button disabled={this.props.disabledZeroButton} onClick={(e) => this.props.onPress(e.target.innerText) } type="button">0</button>
          <button disabled={this.props.disabledOperators || this.props.disabledEqualButton} onClick={(e) => this.props.onPress(e.target.innerText) } type="button">=</button>
          <button disabled={this.props.disabledOperators} onClick={(e) => this.props.onPress(e.target.innerText) } type="button">/</button>
        </div>
      </div>
    );
  }
}

export default KeyBoard;
